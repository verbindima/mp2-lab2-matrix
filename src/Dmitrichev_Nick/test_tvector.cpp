#include "utmatrix.h"

#include <gtest.h>

TEST(TVector, can_create_vector_with_positive_length)
//����������� �������� ������� � ������������� ������
{
  ASSERT_NO_THROW(TVector<int> v(5));
}

TEST(TVector, cant_create_too_large_vector)
//������������� �������� ����� �������� �������
{
  ASSERT_ANY_THROW(TVector<int> v(MAX_VECTOR_SIZE + 1));
}

TEST(TVector, throws_when_create_vector_with_negative_length)
//������������� �������� ������� � ������������� ������
{
  ASSERT_ANY_THROW(TVector<int> v(-5));
}

TEST(TVector, throws_when_create_vector_with_negative_startindex)
//������������� �������� ������� � ������������� ��������� ��������
{
  ASSERT_ANY_THROW(TVector<int> v(5, -2));
}

TEST(TVector, can_create_copied_vector)
//�������� ������������ �����������
{
  TVector<int> v(3);

  ASSERT_NO_THROW(TVector<int> v1(v));
}

TEST(TVector, copied_vector_is_equal_to_source_one)
//������ �� ������������ ����������� ����� ���������
{
	const int size = 3;
	TVector<int> v1(size);
	for (int i = 0;i < size;++i)
		v1[i] = 1;
	TVector<int> v2(v1);

	EXPECT_EQ(v1,v2);
}

TEST(TVector, copied_vector_has_its_own_memory)
//������ �� ������������ ����������� ����� ���� �����
{
	const int size = 3;
	TVector<int> v1(size);
	for (int i = 0;i < size;++i)
		v1[i] = 0;
	TVector<int> v2(v1);

	EXPECT_NE(&v1,&v2);
}

TEST(TVector, can_get_size)
//����������� �������� ������ �������
{
	const int size = 3;
	TVector<int> v(size);

	EXPECT_EQ(size, v.GetSize());
}

TEST(TVector, can_get_start_index)
//����������� �������� ��������� ������
{
  const int st_in = 2;
  TVector<int> v(3, st_in);

  EXPECT_EQ(st_in, v.GetStartIndex());
}

TEST(TVector, can_set_and_get_element)
//����������� ���������� � �������� ������� �������
{
  TVector<int> v(4);
  v[0] = 4;

  EXPECT_EQ(4, v[0]);
}

TEST(TVector, throws_when_set_element_with_negative_index)
//������������� ���������� �������� �������� � ������������� ��������
{
  TVector<int> v(1);

  ASSERT_ANY_THROW(v[-1]=2);
}

TEST(TVector, throws_when_set_element_with_too_large_index)
//������������� ���������� �������� �������� � ����� ������� ��������
{
	TVector<int> v(1);

	ASSERT_ANY_THROW(v[MAX_VECTOR_SIZE+1] = 2);;
}

TEST(TVector, can_assign_vector_to_itself)
//����������� ��������� ������ ������ ����
{
	const int size = 4;
	TVector<int> v(size);
	for (int i = 0;i < size;++i)
		v[i] = 0;

	ASSERT_NO_THROW(v = v);
}

TEST(TVector, can_assign_vectors_of_equal_size)
//����������� ��������� ������ ������� ������� ������ �� �������
{
	const int size = 4;
	TVector<int> v1(size), v2(size);;
	for (int i = 0;i < size;++i)
		v1[i] = 1;

	EXPECT_EQ(v1, v2=v1);
}

TEST(TVector, assign_operator_change_vector_size)
//�������� ������������ ������ ������ �������
{
	const int size = 4;
	TVector<int> v1(size), v2(2*size);;
	for (int i = 0;i < size;++i)
		v2[i] = 1;
	int _size = v1.GetSize();
	v1 = v2;

	EXPECT_NE(v1.GetSize(),_size);
}

TEST(TVector, can_assign_vectors_of_different_size)
//����������� ��������� ������� ������ ������� �������
{
	const int size = 4;
	TVector<int> v1(size), v2(2 * size);;
	for (int i = 0;i < size;++i)
		v1[i] = 1;

	ASSERT_NO_THROW(v2 = v1);
}

TEST(TVector, compare_equal_vectors_return_true)
//��������� ������ ��������
{
	const int size = 4;
	TVector<int> v1(size), v2(size);
	for (int i = 0;i < size;++i)
		v1[i] = v2[i] = 1;

	EXPECT_TRUE(v1 == v2);
}

TEST(TVector, compare_vector_with_itself_return_true)
//��������� ������� � ����� �����
{
	const int size = 4;
	TVector<int> v(size);
	for (int i = 0;i < size;++i)
		v[i] = 1;

	EXPECT_TRUE(v == v);
}

TEST(TVector, vectors_with_different_size_are_not_equal)
//������� ������ ����� �������
{
	const int size = 4;
	TVector<int> v1(size), v2(2*size);
	for (int i = 0;i < size;++i)
		v1[i] = v2[i] = 1;

	EXPECT_FALSE(v1 == v2);;
}

TEST(TVector, can_add_scalar_to_vector)
//����������� ������� � �������
{
	const int size = 4;
	TVector<int> v1(size), v2(size);
	for (int i = 0;i < size;++i)
	{
		v1[i] = v2[i] = 1;
		v2[i] += 2;
	}

	EXPECT_EQ(v1+2,v2);
}

TEST(TVector, can_subtract_scalar_from_vector)
//��������� ������� �� �������
{
	const int size = 4;
	TVector<int> v1(size), v2(size);
	for (int i = 0;i < size;++i)
	{
		v1[i] = v2[i] = 1;
		v2[i] -= 2;
	}

	EXPECT_EQ(v1 - 2, v2);
}

TEST(TVector, can_multiply_scalar_by_vector)
//��������� ������� �� ������
{
	const int size = 4;
	TVector<int> v1(size), v2(size);
	for (int i = 0;i < size;++i)
	{
		v1[i] = v2[i] = 2;
		v2[i] *= 3;
	}

	EXPECT_EQ(v1 * 3, v2);
}

TEST(TVector, can_add_vectors_with_equal_size)
//�������� �������� ������ �����
{
	const int size = 4;
	TVector<int> v1(size), v2(size),v3(size);
	for (int i = 0;i < size;++i)
	{
		v1[i] = v2[i]=1;
		v3[i]=2;
	}

	EXPECT_EQ(v1 +v2, v3);
}

TEST(TVector, cant_add_vectors_with_not_equal_size)
//������������� ������� ������� ������ �����
{
	const int size = 4;
	TVector<int> v1(size), v2(2 * size);
	for (int i = 0;i < size;++i)
		v1[i] = v2[i] = 1;
	for (int i = size;i < 2 * size;++i)
		v2[i] = 1;

	ASSERT_ANY_THROW(v1 + v2);
}

TEST(TVector, can_subtract_vectors_with_equal_size)
//��������� �������� ������ �����
{
	const int size = 4;
	TVector<int> v1(size), v2(size), v3(size);
	for (int i = 0;i < size;++i)
	{
		v1[i] = v2[i] = 1;
		v3[i] = 0;
	}

	EXPECT_EQ(v1 - v2, v3);
}

TEST(TVector, cant_subtract_vectors_with_not_equal_size)
//������������� ������� ������� ������ �����
{
	const int size = 4;
	TVector<int> v1(size), v2(2 * size);
	for (int i = 0;i < size;++i)
		v1[i] = v2[i] = 1;
	for (int i = size;i < 2 * size;++i)
		v2[i] = 1;

	ASSERT_ANY_THROW(v1 - v2);
}

TEST(TVector, can_multiply_vectors_with_equal_size)
//����������� ����� ��������� ������������ �������� � ������ ������
{
	const int size = 4;
	TVector<int> v1(size), v2(size);
	for (int i = 0;i < size;++i)
	{
		v1[i] = 1;
		v2[i] = 0;
	}

	EXPECT_EQ(v1 * v2, 0);
}

TEST(TVector, cant_multiply_vectors_with_not_equal_size)
//������������� ����� ��������� ������������ �������� � ������ ������
{
	const int size = 4;
	TVector<int> v1(size), v2(2*size);
	for (int i = 0;i < size;++i)
		v1[i] = v2[i] = 1;
	for (int i = size;i < 2 * size;++i)
		v2[i] = 1;

	ASSERT_ANY_THROW(v1 * v2);
}

