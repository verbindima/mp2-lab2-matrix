# Методы программирования 2: Верхнетреугольные матрицы на шаблонах


## Цели и задачи


__Цель данной работы__ - В рамках лабораторной работы ставится задача создания программных средств, поддерживающих эффективное хранение матриц специального вида (верхнетреугольных) и выполнение основных операций над ними:
* сложение/вычитание;
* копирование;
* сравнение.

__Задачи:__    
1.Реализация методов шаблонного класса `TVector` согласно заданному интерфейсу.
2.Реализация методов шаблонного класса `TMatrix` согласно заданному интерфейсу.
3.Реализация заготовок тестов, покрывающих все методы классов `TVector` и `TMatrix`.
4.Обеспечение работоспособности тестов и примера использования.
5.Модификация примера использования в тестовое приложение, позволяющее задавать матрицы и осуществлять основные операции над ними.

## Начало работы:

Классы `TVector` и `TMatrix` (`TMatrix` наследует класс `TVector`) реализуются как шаблоны, поэтому объявление и реализация классов находятся в одном файле.
Константы ограничивающие размеры создаваемых объектах:
```c++
const int MAX_VECTOR_SIZE = 100000000;
const int MAX_MATRIX_SIZE = 10000;
```
В качестве обработки ошибок используется исключения `throw`:
* throw _1_ - выбрасывается исключение в случае ввода некорректной длины вектора.
* throw _2_ - выбрасывается исключение в случае ввода некорректной переменны _StartIndex_.
* throw _3_ - выбрасывается исключение в случае выхода за границу вектора.
* throw _4_ - выбрасывается исключение в случае когда размеры векторов не совпадают.
* throw _5_ - выбрасывается исключение в случае ввода некорректного размера матрицы.

### 1. Шаблон класса TVector:

#### Объявление:
```c++
// Шаблон вектора
template <class ValType>
class TVector
{
protected:
  ValType *pVector;
  int Size;       // размер вектора
  int StartIndex; // индекс первого элемента вектора
public:
  TVector(int s = 10, int si = 0);
  TVector(const TVector &v);                // конструктор копирования
  ~TVector();
  int GetSize()      { return Size;       } // размер вектора
  int GetStartIndex(){ return StartIndex; } // индекс первого элемента
  ValType& operator[](int pos);             // доступ
  bool operator==(const TVector &v) const;  // сравнение
  bool operator!=(const TVector &v) const;  // сравнение
  TVector& operator=(const TVector &v);     // присваивание

  // скалярные операции
  TVector  operator+(const ValType &val);   // прибавить скаляр
  TVector  operator-(const ValType &val);   // вычесть скаляр
  TVector  operator*(const ValType &val);   // умножить на скаляр

  // векторные операции
  TVector  operator+(const TVector &v);     // сложение
  TVector  operator-(const TVector &v);     // вычитание
  ValType  operator*(const TVector &v);     // скалярное произведение

  // ввод-вывод
  friend istream& operator>>(istream &in, TVector &v)
  {
    for (int i = 0; i < v.Size; i++)
      in >> v.pVector[i];
    return in;
  }
  friend ostream& operator<<(ostream &out, const TVector &v)
  {
    for (int i = 0; i < v.Size; i++)
      out << v.pVector[i] << ' ';
    return out;
  }
};
```

#### Реализация:
```c++
template <class ValType>
TVector<ValType>::TVector(int s, int si)
{
	if (s <= 0 || s > MAX_VECTOR_SIZE) throw 1; //"Error: Incorrect vector length!"
	if (si < 0 || si >= MAX_VECTOR_SIZE) throw 2; //"Error: Incorrect start index!"
	pVector = new ValType[s];
	Size = s;
	StartIndex = si;		
} /*-------------------------------------------------------------------------*/

template <class ValType> //конструктор копирования
TVector<ValType>::TVector(const TVector<ValType> &v)
{	
	Size = v.Size;
	StartIndex = v.StartIndex;
	pVector = new ValType[Size];
	for (int i = 0; i < Size; i++)
		pVector[i] = v.pVector[i];
} /*-------------------------------------------------------------------------*/

template <class ValType>
TVector<ValType>::~TVector()
{
	delete[] pVector;
} /*-------------------------------------------------------------------------*/

template <class ValType> // доступ
ValType& TVector<ValType>::operator[](int pos)
{
	if ((pos-StartIndex) < 0 || (pos - StartIndex) >= Size) throw 3; //"Error: incorrect vector index!"
	return pVector[pos - StartIndex];
} /*-------------------------------------------------------------------------*/

template <class ValType> // сравнение
bool TVector<ValType>::operator==(const TVector &v) const
{
	bool val = false;
	if (Size == v.Size)
	{
		val = true;
		for (int i = 0; i < Size; i++)
			if (pVector[i] != v.pVector[i]) { val = false; break; }
	}

	return val;
} /*-------------------------------------------------------------------------*/

template <class ValType> // сравнение
bool TVector<ValType>::operator!=(const TVector &v) const
{
	return !(*this == v);
} /*-------------------------------------------------------------------------*/

template <class ValType> // присваивание
TVector<ValType>& TVector<ValType>::operator=(const TVector &v)
{
	if (*this != v)
	{
		delete[] pVector;
		Size = v.Size;
		StartIndex = v.StartIndex;
		pVector = new ValType[Size];
		for (int i = 0; i < Size; i++)
			pVector[i] = v.pVector[i];		
	}

	return *this;
} /*-------------------------------------------------------------------------*/

template <class ValType> // прибавить скаляр
TVector<ValType> TVector<ValType>::operator+(const ValType &val)
{
	TVector<ValType> tmp(*this);
	for (int i = 0; i < Size; i++)
		tmp.pVector[i] += val;
	return tmp;
} /*-------------------------------------------------------------------------*/

template <class ValType> // вычесть скаляр
TVector<ValType> TVector<ValType>::operator-(const ValType &val)
{
	TVector<ValType> tmp(*this);
	for (int i = 0; i < Size; i++)
		tmp.pVector[i] -= val;
	return tmp;
} /*-------------------------------------------------------------------------*/

template <class ValType> // умножить на скаляр
TVector<ValType> TVector<ValType>::operator*(const ValType &val)
{
	TVector<ValType> tmp(*this);
	for (int i = 0; i < Size; i++)
		tmp.pVector[i] *= val;
	return tmp;
} /*-------------------------------------------------------------------------*/

template <class ValType> // сложение
TVector<ValType> TVector<ValType>::operator+(const TVector<ValType> &v)
{
	if (Size != v.Size) throw 4; //"Error: Size not match!"
	TVector<ValType> tmp(*this);
	for (int i = 0; i < Size; i++)
		tmp.pVector[i] = pVector[i] + v.pVector[i];
	return tmp;
} /*-------------------------------------------------------------------------*/

template <class ValType> // вычитание
TVector<ValType> TVector<ValType>::operator-(const TVector<ValType> &v)
{
	if (Size != v.Size) throw 4; //"Error: Size not match!"
	TVector<ValType> tmp(*this);
	for (int i = 0; i < Size; i++)
		tmp.pVector[i] = pVector[i] - v.pVector[i];
	return tmp;
} /*-------------------------------------------------------------------------*/

template <class ValType> // скалярное произведение
ValType TVector<ValType>::operator*(const TVector<ValType> &v)
{
	if(Size != v.Size) throw 4; //"Error: Size not match!"
	ValType result = pVector[0] * v.pVector[0];
	for(int i = 1; i < Size; i++)
		result += pVector[i] * v.pVector[i];
	return result;
} /*-------------------------------------------------------------------------*/
```

### 2. Шаблон класса TMatrix:

#### Объявление:

```c++
// Верхнетреугольная матрица
template <class ValType>
class TMatrix : public TVector<TVector<ValType> >
{
public:
  TMatrix(int s = 10);                           
  TMatrix(const TMatrix &mt);                    // копирование
  TMatrix(const TVector<TVector<ValType> > &mt); // преобразование типа
  bool operator==(const TMatrix &mt) const;      // сравнение
  bool operator!=(const TMatrix &mt) const;      // сравнение
  TMatrix& operator= (const TMatrix &mt);        // присваивание
  TMatrix  operator+ (const TMatrix &mt);        // сложение
  TMatrix  operator- (const TMatrix &mt);        // вычитание

  // ввод / вывод
  friend istream& operator>>(istream &in, TMatrix &mt)
  {
    for (int i = 0; i < mt.Size; i++)
      in >> mt.pVector[i];
    return in;
  }
  friend ostream & operator<<( ostream &out, const TMatrix &mt)
  {
    for (int i = 0; i < mt.Size; i++)
      out << mt.pVector[i] << endl;
    return out;
  }
};
```

#### Реализация:
```c++
template <class ValType>
TMatrix<ValType>::TMatrix(int s): TVector<TVector<ValType> >(s)
{
	if (s <= 0 || s > MAX_MATRIX_SIZE) throw 5; //"Error: Incorrect matrix length!"	
	for (int i = 0; i < s; i++)
		pVector[i] = TVector<ValType>(s - i, i);
} /*-------------------------------------------------------------------------*/

template <class ValType> // конструктор копирования
TMatrix<ValType>::TMatrix(const TMatrix<ValType> &mt):
  TVector<TVector<ValType> >(mt) {}

template <class ValType> // конструктор преобразования типа
TMatrix<ValType>::TMatrix(const TVector<TVector<ValType> > &mt):
  TVector<TVector<ValType> >(mt) {}

template <class ValType> // сравнение
bool TMatrix<ValType>::operator==(const TMatrix<ValType> &mt) const
{
	return TVector<TVector<ValType>>::operator==(mt);
} /*-------------------------------------------------------------------------*/

template <class ValType> // сравнение
bool TMatrix<ValType>::operator!=(const TMatrix<ValType> &mt) const
{
	return !(*this == mt);
} /*-------------------------------------------------------------------------*/

template <class ValType> // присваивание
TMatrix<ValType>& TMatrix<ValType>::operator=(const TMatrix<ValType> &mt)
{
	return TVector<TVector<ValType>>::operator=(mt);
} /*-------------------------------------------------------------------------*/

template <class ValType> // сложение
TMatrix<ValType> TMatrix<ValType>::operator+(const TMatrix<ValType> &mt)
{
	return TVector<TVector<ValType>>::operator+(mt);
} /*-------------------------------------------------------------------------*/

template <class ValType> // вычитание
TMatrix<ValType> TMatrix<ValType>::operator-(const TMatrix<ValType> &mt)
{
	return TVector<TVector<ValType>>::operator-(mt);
} /*-------------------------------------------------------------------------*/
```

### 3. Реализация тестов:

В данной работе используется фреймворк для написания автоматических тестов __Google Test__.

#### 3.1 Тесты для класса __TVector__:

```c++
#include "utmatrix.h"

#include "gtest.h"

TEST(TVector, can_create_vector_with_positive_length)
{
	ASSERT_NO_THROW(TVector<int> v(5));
}

TEST(TVector, cant_create_too_large_vector)
{
	ASSERT_ANY_THROW(TVector<int> v(MAX_VECTOR_SIZE + 1));
}

TEST(TVector, throws_when_create_vector_with_negative_length)
{
	ASSERT_ANY_THROW(TVector<int> v(-5));
}

TEST(TVector, throws_when_create_vector_with_negative_startindex)
{
	ASSERT_ANY_THROW(TVector<int> v(5, -2));
}

TEST(TVector, can_create_copied_vector)
{
	TVector<int> v(10);

	ASSERT_NO_THROW(TVector<int> v1(v));
}

TEST(TVector, copied_vector_is_equal_to_source_one)
{
	const int size = 4;
	TVector<int> v1(size);
	for (int i = 0; i < size; i++)
		v1[i] = 0;

	TVector<int> v2(v1);

	EXPECT_EQ(v1, v2);
}

TEST(TVector, copied_vector_has_its_own_memory)
{
	const int size = 4;
	TVector<int> v1(size);
	for (int i = 0; i < size; i++)
		v1[i] = 0;

	TVector<int> v2(v1);
	v1[0] = 1;

	EXPECT_NE(v1, v2);
}

TEST(TVector, can_get_size)
{
	TVector<int> v(4);

	EXPECT_EQ(4, v.GetSize());
}

TEST(TVector, can_get_start_index)
{
	TVector<int> v(4, 2);

	EXPECT_EQ(2, v.GetStartIndex());
}

TEST(TVector, can_set_and_get_element)
{
	TVector<int> v(4);
	v[0] = 4;

	EXPECT_EQ(4, v[0]);
}

TEST(TVector, throws_when_set_element_with_negative_index)
{
	TVector<int> v(4);	

	ASSERT_ANY_THROW(v[-1] = 0);
}

TEST(TVector, throws_when_set_element_with_too_large_index)
{
	TVector<int> v(4);

	ASSERT_ANY_THROW(v[6] = 0);
}

TEST(TVector, can_assign_vector_to_itself)
{
	const int size = 4;
	TVector<int> v(size);
	for (int i = 0; i < size; i++)
		v[i] = 0;

	ASSERT_NO_THROW(v = v);
}

TEST(TVector, can_assign_vectors_of_equal_size)
{
	const int size = 4;
	TVector<int> v1(size), v2(size);
	for (int i = 0; i < size; i++)
		v2[i] = 0;

	ASSERT_NO_THROW(v1 = v2);
}

TEST(TVector, assign_operator_change_vector_size)
{
	const int size_1 = 4, size_2 = 6;
	TVector<int> v1(size_1), v2(size_2);
	for (int i = 0; i < size_2; i++)
		v2[i] = 0;

	v1 = v2;

	EXPECT_NE(size_1, v1.GetSize());
}

TEST(TVector, can_assign_vectors_of_different_size)
{
	const int size_1 = 4, size_2 = 6;
	TVector<int> v1(size_1), v2(size_2);
	for (int i = 0; i < size_2; i++)
		v2[i] = 0;
	
	ASSERT_NO_THROW(v1 = v2);
}

TEST(TVector, compare_equal_vectors_return_true)
{
	const int size = 4;
	TVector<int> v1(size);
	for (int i = 0; i < size; i++)	
		v1[i] = 0;
	TVector<int> v2(v1);

	EXPECT_TRUE(v1 == v2);
}

TEST(TVector, compare_vector_with_itself_return_true)
{
	const int size = 4;
	TVector<int> v(size);
	for (int i = 0; i < size; i++)
		v[i] = 0;

	EXPECT_TRUE(v == v);
}

TEST(TVector, vectors_with_different_size_are_not_equal)
{
	const int size_1 = 4, size_2 = 6;
	TVector<int> v1(size_1), v2(size_2);
	for (int i = 0; i < size_2; i++)
	{
		if(i < size_1)
			v1[i] = 0;
		v2[i] = 0;
	}

	EXPECT_TRUE(v1 != v2);
}

TEST(TVector, can_add_scalar_to_vector)
{
	const int size = 4;
	TVector<int> v1(size), v2(size);
	for (int i = 0; i < size; i++)
	{
		v1[i] = 0;
		v2[i] = 4;
	}
	
	EXPECT_EQ(v1 + 4, v2);
}

TEST(TVector, can_subtract_scalar_from_vector)
{
	const int size = 4;
	TVector<int> v1(size), v2(size);
	for (int i = 0; i < size; i++)
	{
		v1[i] = 4;
		v2[i] = 0;
	}

	EXPECT_EQ(v1 - 4, v2);
}

TEST(TVector, can_multiply_scalar_by_vector)
{
	const int size = 4;
	TVector<int> v1(size), v2(size);
	for (int i = 0; i < size; i++)
	{
		v1[i] = 2;
		v2[i] = 4;
	}

	EXPECT_EQ(v1 * 2, v2);
}

TEST(TVector, can_add_vectors_with_equal_size)
{
	const int size = 4;
	TVector<int> v1(size), v2(size), v3(size);
	for (int i = 0; i < size; i++)
	{
		v1[i] = 2;
		v2[i] = 2;
		v3[i] = 4;
	}

	EXPECT_EQ(v1 + v2, v3);
}

TEST(TVector, cant_add_vectors_with_not_equal_size)
{
	const int size_1 = 4, size_2 = 6;
	TVector<int> v1(size_1), v2(size_2);
	for (int i = 0; i < size_2; i++)
	{
		if (i < size_1)
			v1[i] = 1;
		v2[i] = 1;
	}

	ASSERT_ANY_THROW(v1 + v2);
}

TEST(TVector, can_subtract_vectors_with_equal_size)
{
	const int size = 4;
	TVector<int> v1(size), v2(size), v3(size);
	for (int i = 0; i < size; i++)
	{
		v1[i] = 2;
		v2[i] = 2;
		v3[i] = 0;
	}

	EXPECT_EQ(v1 - v2, v3);
}

TEST(TVector, cant_subtract_vectors_with_not_equal_size)
{
	const int size_1 = 4, size_2 = 6;
	TVector<int> v1(size_1), v2(size_2);
	for (int i = 0; i < size_2; i++)
	{
		if (i < size_1)
			v1[i] = 1;
		v2[i] = 1;
	}

	ASSERT_ANY_THROW(v1 - v2);
}

TEST(TVector, can_multiply_vectors_with_equal_size)
{
	const int size = 4;
	TVector<int> v1(size), v2(size);
	for (int i = 0; i < size; i++)	
		v1[i] = v2[i] = 2;

	EXPECT_EQ(16, v1 * v2);
}

TEST(TVector, cant_multiply_vectors_with_not_equal_size)
{
	const int size_1 = 4, size_2 = 6;
	TVector<int> v1(size_1), v2(size_2);
	for (int i = 0; i < size_2; i++)
	{
		if (i < size_1)
			v1[i] = 2;
		v2[i] = 2;
	}

	ASSERT_ANY_THROW(v1 * v2);
}
```
#### 3.2 Тесты для класса __TMatrix__:

```c++
#include "utmatrix.h"

#include "gtest.h"

TEST(TMatrix, can_create_matrix_with_positive_length)
{
	ASSERT_NO_THROW(TMatrix<int> m(5));
}

TEST(TMatrix, cant_create_too_large_matrix)
{
	ASSERT_ANY_THROW(TMatrix<int> m(MAX_MATRIX_SIZE + 1));
}

TEST(TMatrix, throws_when_create_matrix_with_negative_length)
{
	ASSERT_ANY_THROW(TMatrix<int> m(-5));
}

TEST(TMatrix, can_create_copied_matrix)
{
	TMatrix<int> m(5);

	ASSERT_NO_THROW(TMatrix<int> m1(m));
}

TEST(TMatrix, copied_matrix_is_equal_to_source_one)
{
	const int size = 3;
	TMatrix<int> m1(size);
	for (int i = 0; i < size; i++)
		for (int j = i; j < size; j++)
			m1[i][j] = 0;

	TMatrix<int> m2(m1);
	
	EXPECT_EQ(m1, m2);
}

TEST(TMatrix, copied_matrix_has_its_own_memory)
{
	const int size = 3;
	TMatrix<int> m1(size);
	for (int i = 0; i < size; i++)
		for (int j = i; j < size; j++)
			m1[i][j] = 0;

	TMatrix<int> m2(m1);
	m1[0][0] = 1;

	EXPECT_NE(m1, m2);
}

TEST(TMatrix, can_get_size)
{
	TMatrix<int> m(5);

	EXPECT_EQ(5, m.GetSize());
}

TEST(TMatrix, can_set_and_get_element)
{
	TMatrix<int> m(5);
	m[0][0] = 5;

	EXPECT_EQ(5, m[0][0]);
}

TEST(TMatrix, throws_when_set_element_with_negative_index)
{
	TMatrix<int> m(5);

	ASSERT_ANY_THROW(m[-1] = 0);
}

TEST(TMatrix, throws_when_set_element_with_too_large_index)
{
	TMatrix<int> m(5);

	ASSERT_ANY_THROW(m[7] = 0);
}

TEST(TMatrix, can_assign_matrix_to_itself)
{
	const int size = 5;
	TMatrix<int> m(size);
	for (int i = 0; i < size; i++)
		for (int j = i; j < size; j++)
			m[i][j] = 0;

	ASSERT_NO_THROW(m = m);
}

TEST(TMatrix, can_assign_matrices_of_equal_size)
{
	const int size = 5;
	TMatrix<int> m1(size), m2(size);
	for (int i = 0; i < size; i++)
		for (int j = i; j < size; j++)
			m2[i][j] = 0;

	ASSERT_NO_THROW(m1 = m2);
}

TEST(TMatrix, assign_operator_change_matrix_size)
{
	const int size_1 = 5, size_2 = 7;
	TMatrix<int> m1(size_1), m2(size_2);
	for (int i = 0; i < size_2; i++)
		for (int j = i; j < size_2; j++)
			m2[i][j] = 0;

	m1 = m2;

	EXPECT_NE(size_1, m1.GetSize());
}

TEST(TMatrix, can_assign_matrices_of_different_size)
{
	const int size_1 = 5, size_2 = 7;
	TMatrix<int> m1(size_1), m2(size_2);
	for (int i = 0; i < size_2; i++)
		for (int j = i; j < size_2; j++)
			m2[i][j] = 0;

	
	ASSERT_NO_THROW(m1 = m2);
}

TEST(TMatrix, compare_equal_matrices_return_true)
{
	const int size = 5;
	TMatrix<int> m1(size);
	for (int i = 0; i < size; i++)
		for (int j = i; j < size; j++)
			m1[i][j] = 0;

	TMatrix<int> m2(m1);

	EXPECT_TRUE(m1 == m2);
}

TEST(TMatrix, compare_matrix_with_itself_return_true)
{
	const int size = 5;
	TMatrix<int> m(size);
	for (int i = 0; i < size; i++)
		for (int j = i; j < size; j++)
			m[i][j] = 0;	

	EXPECT_TRUE(m == m);
}

TEST(TMatrix, matrices_with_different_size_are_not_equal)
{
	const int size_1 = 5, size_2 = 7;
	TMatrix<int> m1(size_1), m2(size_2);	

	EXPECT_TRUE(m1 != m2);
}

TEST(TMatrix, can_add_matrices_with_equal_size)
{
	const int size = 5;
	TMatrix<int> m1(size), m2(size), m3(size);
	for (int i = 0; i < size; i++)
		for (int j = i; j < size; j++)
		{
			m1[i][j] = 2;
			m2[i][j] = 2;
			m3[i][j] = 4;
		}

	EXPECT_EQ(m1 + m2, m3);
}

TEST(TMatrix, cant_add_matrices_with_not_equal_size)
{
	const int size_1 = 5, size_2 = 7;
	TMatrix<int> m1(size_1), m2(size_2);
	
	ASSERT_ANY_THROW(m1 + m2);
}

TEST(TMatrix, can_subtract_matrices_with_equal_size)
{
	const int size = 5;
	TMatrix<int> m1(size), m2(size), m3(size);
	for (int i = 0; i < size; i++)
		for (int j = i; j < size; j++)
		{
			m1[i][j] = 2;
			m2[i][j] = 2;
			m3[i][j] = 0;
		}

	EXPECT_EQ(m1 - m2, m3);
}

TEST(TMatrix, cant_subtract_matrixes_with_not_equal_size)
{
	const int size_1 = 5, size_2 = 7;
	TMatrix<int> m1(size_1), m2(size_2);
	
	ASSERT_ANY_THROW(m1 - m2);
}
```

### 4. Обеспечение работоспособности тестов:

#### 4.1 Прохождение тестов для класса __TVector__:

![](http://s05.radikal.ru/i178/1610/c0/afb7934d255c.png)

Все тесты успешно пройдены!

#### 4.2 Прохождение тестов для класса __TMatrix__:

![](http://s017.radikal.ru/i418/1610/12/e99765f8587c.png)

Все тесты успешно пройдены!

### 5. Модификация примера использования:

#### Исходный код тестового приложения:

```c++
#include <iostream>
#include "utmatrix.h"
//---------------------------------------------------------------------------

void main()
{
	try
	{		
		int i, j, size;		

		setlocale(LC_ALL, "Russian");
		cout << "Тестирование программ поддержки представления треугольных матриц \nВведите размер матриц: ";
		cin >> size;
		TMatrix<int> a(size), b(size), c(size);

		cout << "Введите матрицу a:" << endl;
		cin >> a;

		cout << "Введите матрицу b:" << endl;
		cin >> b;
  
		cout << "Matrix a = " << endl << a << endl;
		cout << "Matrix b = " << endl << b << endl;

		c = a + b;
		cout << "Matrix c = a + b" << endl << c << endl;

		c = a - b;
		cout << "Matrix c = a - b" << endl << c << endl;

		c = b;
		cout << "Matrix c = b" << endl << c << endl;
		cout << "c == b" << endl << (c == b) << endl;
		cout << "c == a" << endl << (c == a) << endl;

	}
	catch (int err)
	{
		cout << "Error №" << err << endl;
	}
}
//---------------------------------------------------------------------------
```

#### Результат работы тестового приложения:

![](http://s017.radikal.ru/i402/1610/a1/8eaa73c06340.png)

# Вывод:

В данной работе были реализованы два класса:
* __TVector__ для работы с векторами.
* __TMatrix__ для работы с верхнетреугольными матрицами. Причем __TMatrix__ наследует класс __TVector__.

Оба класса реализованы как шаблоны, что предоставляет гибкость в их использовании. 
При помощи шаблонов появилась возможность использовать произвольный тип данных используемых в матрице.

Так же была использована система тестирования __Google Test__ и реализованны собственные тесты.
Успешное проходение тестов дает уверенность в том что классы реализованы правильно и при их использовании, программы будут корректно выдавать результат.